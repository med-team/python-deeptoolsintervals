from deeptoolsintervals import GTF
from deeptoolsintervals import Enrichment

# Very basic test to check functionality
def test_functionality():
    gtf = GTF(["GRCh38.84.2.gtf.gz", "GRCh38.84.bed", "GRCh38.84.gtf.gz"], keepExons=True)
    list_labels = gtf.labels
    list_labels.sort()
    assert(list_labels == ['GRCh38.84.2.gtf.gz', 'GRCh38.84.bed', 'GRCh38.84.gtf.gz', 'group 1', 'group 1_r1', 'group 2', 'group 3', 'group2'])
    
    gtf = GTF("GRCh38.84.gtf.gz", keepExons=True)
    assert(gtf.findOverlaps("chr1", 1, 20000)[0][0] >= 11800)

    gtf = Enrichment("GRCh38.84.gtf.gz", keepExons=True, attributeKey="gene_biotype")
    overlap_result = list(gtf.findOverlaps("1", [(0, 2000000)]))
    overlap_result.sort()
    assert(overlap_result == 
        ['group 1', 'group 2', 'lincRNA', 'miRNA', 'processed_pseudogene', 'protein_coding', 'transcribed_unprocessed_pseudogene', 'unprocessed_pseudogene'])
