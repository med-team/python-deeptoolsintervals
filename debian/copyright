Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: deeptools_intervals
Source: https://github.com/deeptools/deeptools_intervals

Files: *
Copyright: 2018-2019 Devon Ryan
                     Max Planck Institute for Immunobiology and Epigenetics,
                     Freiburg, Germany
License: MIT

Files: deeptoolsintervals/tree/k*.h
Copyright: 2008, 2009, 2011 Heng Li Attractive Chaos <attractor@live.co.uk>
License: MIT

Files: deeptoolsintervals/tree/murmur3.*
Copyright: 2016 Austin Appleby
License: PublicDomain
 MurmurHash3 was written by Austin Appleby, and is placed in the public
 domain. The author hereby disclaims copyright to this source code.

Files: debian/*
Copyright: 2019 Steffen Moeller <moeller@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
